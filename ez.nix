{
  network.description = "ezbackend";

  ez = { config, pkgs, resources, ... }:
  let
    pgrst = import ./pgrst.nix { stdenv = pkgs.stdenv; fetchurl = pkgs.fetchurl; };
  in
  {
    environment.systemPackages = [ pgrst ];

    services.nginx = {
      enable = true;
      config = ''
        events {
          worker_connections 768;
        }

        http {

          upstream postgrest {
            server localhost:3000;
          }

          include ${pkgs.nginx}/conf/mime.types;

          server {
            listen 80;

            index index.html;

            root ${./front};

            location / {
              try_files $uri $uri/ =404;
            }

            location /demo/ {
              proxy_pass http://localhost:8080/;
            }

            location /api/ {
              default_type  application/json;
              proxy_pass http://postgrest/;
            }
          }
        }
      '';
    };
    services.postgresql = {
      enable = true;
      package = pkgs.postgresql_11;
      authentication = pkgs.lib.mkOverride 10 ''
        local   all all trust
        host    all all 127.0.0.1/32 trust
        host    all all ::1/128 trust
      '';
      extraConfig = ''
        timezone = 'America/Lima'
      '';
      initialScript = ./world.sql;
    };
    systemd.services.postgrest = {
      enable      = true;
      description = "postgrest daemon";
      after       = [ "postgresql.service" ];
      wantedBy    = [ "multi-user.target" ];
      serviceConfig.ExecStart = "${pgrst}/bin/postgrest ${./pgrst.conf}";
    };

    networking.firewall.allowedTCPPorts = [ 80 ];
    virtualisation.docker.enable = true;
    ## docker run -p 8080:8080 -e API_URL="http://ec2-3-15-181-82.us-east-2.compute.amazonaws.com/api" -d swaggerapi/swagger-ui
  };
}
